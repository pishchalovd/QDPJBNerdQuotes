package ca.qc.dawsoncollege.qdpjbnerdquotes;

import android.os.Bundle;

/*
* @author Denys Pishchalov
* @author Jonathan Bizier
* */
public class AboutActivity extends OptionsMenu {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
