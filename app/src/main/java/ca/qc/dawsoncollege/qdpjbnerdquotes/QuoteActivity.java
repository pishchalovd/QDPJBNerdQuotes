package ca.qc.dawsoncollege.qdpjbnerdquotes;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/*
* @author Denys Pishchalov
* @author Jonathan Bizier
* */
public class QuoteActivity extends OptionsMenu {

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote);

        intent = getIntent();
        displayQuote(intent);
    }

    /*
    * Sets up everything in the activity according to the extras and sets
    * itself in the SharedPreferences as the last run quote.
    *
    * @param intent
    * */
    public void displayQuote(Intent intent) {
        TextView quoteText = (TextView) findViewById(R.id.quoteText);
        TextView author = (TextView) findViewById(R.id.quoteAuthor);
        TextView date = (TextView) findViewById(R.id.quoteDate);
        TextView source = (TextView) findViewById(R.id.quoteSource);

        quoteText.setText(intent.getStringExtra("quotetext"));
        author.setText("- " + intent.getStringExtra("quoteauthor"));
        date.setText("Date added: " + intent.getStringExtra("quotedate"));
        source.setText(intent.getStringExtra("quotesource"));

        SharedPreferences sp = getSharedPreferences("mypref", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        editor.putString("quotetext", intent.getStringExtra("quotetext"));
        editor.putString("quoteauthor", intent.getStringExtra("quoteauthor"));
        editor.putString("quotedate", intent.getStringExtra("quotedate"));
        editor.putString("quotesource", intent.getStringExtra("quotesource"));
        editor.putString("quoteblurb", intent.getStringExtra("quoteblurb"));
        editor.commit();
    }

    /*
    * Displays general information about author with a dismiss button.
    *
    * @param view
    * */
    public void showBlurbDialog(View view) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.dialog_blurb)
                .setMessage(intent.getStringExtra("quoteblurb"))
                .setNegativeButton(R.string.dialog_dismiss, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).show();
    }
}
