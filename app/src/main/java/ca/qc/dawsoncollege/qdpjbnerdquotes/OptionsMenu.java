package ca.qc.dawsoncollege.qdpjbnerdquotes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/*
* @author Denys Pishchalov
* @author Jonathan Bizier
* */
public class OptionsMenu extends AppCompatActivity {

    protected DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                showAbout();
                break;
            case R.id.random:
                showRandom();
                break;
            case R.id.lastrun:
                showLastRun();
                break;
        }
        return true;
    }

    /*
    * Shows the about activity
    * and kills itself if not from the MainActivity.
    * */
    public void showAbout() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);

        // Kill if not launched from MainActivity to get rid of infinite windows
        if (this.getClass() != MainActivity.class) {
            finish();
        }
    }

    /*
    * Shows a random quote from all categories
    * and kills itself if not from the MainActivity.
    * */
    public void showRandom() {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long categoriesLength = dataSnapshot.getChildrenCount();
                int rand1 = (int) (Math.random() * categoriesLength);
                ArrayList<String> categories = new ArrayList();

                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    categories.add(snap.getKey());
                }
                String category = categories.get(rand1);
                ArrayList<Quotes> quoteList = new ArrayList();
                long quotesLength = dataSnapshot.child(category).getChildrenCount();
                for (DataSnapshot snap : dataSnapshot.child(category).getChildren()) {
                    quoteList.add(snap.getValue(Quotes.class));
                }
                int rand2 = (int) (Math.random() * quotesLength);
                Quotes quote = quoteList.get(rand2);
                startQuoteActivity(quote);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("loadQuote:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
    }

    /*
    * Shows the last quote drawn on screen and gets it from the SharedPreferences
    * and kills itself if not from the MainActivity to avoid infinite window creation.
    * */
    public void showLastRun() {
        Intent intent = new Intent(this, QuoteActivity.class);
        SharedPreferences sp = getSharedPreferences("mypref", MODE_PRIVATE);
        intent.putExtra("quotetext", sp.getString("quotetext", "none"));
        intent.putExtra("quoteauthor", sp.getString("quoteauthor", "none"));
        intent.putExtra("quotedate", sp.getString("quotedate", "none"));
        intent.putExtra("quotesource", sp.getString("quotesource", "none"));
        intent.putExtra("quoteblurb", sp.getString("quoteblurb", "none"));

        // Kill if not launched from MainActivity to get rid of infinite windows
        if (this.getClass() != MainActivity.class) {
            finish();
        }

        startActivityForResult(intent, 1);
    }

    /*
    * General method used to start a quote activity
    * and kills itself if not from the MainActivity to avoid infinite window creation.
    * */
    public void startQuoteActivity(Quotes quote) {
        Intent intent = new Intent(this, QuoteActivity.class);
        intent.putExtra("quotetext", quote.getQuote());
        intent.putExtra("quoteauthor", quote.getAttributed());
        intent.putExtra("quotedate", quote.getDate());
        intent.putExtra("quotesource", quote.getReference());
        intent.putExtra("quoteblurb", quote.getBlurb());

        // Kill if not launched from MainActivity to get rid of infinite windows
        if (this.getClass() != MainActivity.class) {
            finish();
        }

        startActivity(intent);
    }
}
