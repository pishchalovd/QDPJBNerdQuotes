package ca.qc.dawsoncollege.qdpjbnerdquotes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import android.view.ViewGroup.LayoutParams;

/*
* @author Jonathan Bizier
* */
public class QuoteAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    ArrayList<String> categories;
    ListView listView;
    Context context;

    public QuoteAdapter(MainActivity context, ListView listView, ArrayList<String> categories) {
        this.categories = categories;
        this.context = context;
        this.listView = listView;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Item item = new Item();

        View row = inflater.inflate(R.layout.activity_listview, null);

        item.category = (TextView) row.findViewById(R.id.category);
        item.category.setText(categories.get(position));

        item.category.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, listView.getHeight() / 5 - 4)); // Minus 4 because of dividers (4 seems to get rid of the footer)

        return row;
    }

    public class Item {
        TextView category;
    }
}