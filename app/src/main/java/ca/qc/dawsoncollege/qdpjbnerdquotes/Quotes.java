package ca.qc.dawsoncollege.qdpjbnerdquotes;

/*
* @author Denys Pishchalov
* */
public class Quotes {

    private String Attributed;
    private String Blurb;
    private String Date;
    private String Quote;
    private String Reference;


    public Quotes(String attributed, String blurb, String date, String quote, String reference)

    {
        this.Attributed = attributed;
        this.Blurb = blurb;
        this.Date = date;
        this.Quote = quote;
        this.Reference = reference;
    }

    public Quotes()

    {
        this.Attributed = "Attributed";
        this.Blurb = "Blurb";
        this.Date = "Date";
        this.Quote = "Quote";
        this.Reference = "Reference";
    }

    public String getAttributed() {
        return Attributed;
    }

    public void setAttributed(String attributed) {
        this.Attributed = attributed;
    }

    public String getBlurb() {
        return Blurb;
    }

    public void setBlurb(String blurb) {
        this.Blurb = blurb;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        this.Date = date;
    }

    public String getQuote() {
        return Quote;
    }

    public void setQuote(String quote) {
        this.Quote = quote;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        this.Reference = reference;
    }
}
