package ca.qc.dawsoncollege.qdpjbnerdquotes;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/*
* @author Denys Pishchalov
* @author Jonathan Bizier
* */
public class MainActivity extends OptionsMenu {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectDb();
        createListView();
    }

    /*
    * Connects to the firebase database
    * */
    public void connectDb() {
        String email = "pishchalovd3@gmail.com";
        String pass = "dawsondawson123";
        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseAuth.signInWithEmailAndPassword(email, pass);
    }

    /*
    * Get and fill the ListView with categories fetched
    * from the firebase database
    * */
    public void createListView() {
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> categories = new ArrayList<String>();

                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    categories.add(snap.getKey());
                }

                ListView listView = (ListView) findViewById(R.id.categories);
                listView.setAdapter(new QuoteAdapter(MainActivity.this, listView, categories));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("loadQuote:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
    }

    /*
    * Get random quote according to category selected
    * in the ListView
    *
    * @param view
    * */
    public void getRandomQuote(View view) {
        final String category = ((TextView) view).getText().toString();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                ArrayList<Quotes> quoteList = new ArrayList();
                long quotesLength = dataSnapshot.child(category).getChildrenCount();
                if (quotesLength > 0) {
                    Log.i(this.getClass().getName(), "Data Snapshot created successfully");
                    for (DataSnapshot snap : dataSnapshot.child(category).getChildren()) {
                        quoteList.add(snap.getValue(Quotes.class));
                    }

                    int rand = (int) (Math.random() * quotesLength);
                    Quotes quote = quoteList.get(rand);
                    startQuoteActivity(quote);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("loadQuote:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
    }
}
